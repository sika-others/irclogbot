#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "irclogbot",
    version = "1.0.0",
    url = 'https://github.com/ondrejsika/irclogbot/',
    license = 'MIT',
    description = "Python IRC Log Bot",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    scripts = ["irclogbot"],
    install_requires = ["twisted"],
    include_package_data = True,
    zip_safe = False,
)
